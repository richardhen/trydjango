from django import forms

from .models import Product


class ProductForm(forms.ModelForm):
    title = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "Your title here"})
    )
    # email = forms.EmailField()
    description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "placeholder": "Your description",
                "class": "new-class-name two",
                "rows": 20,
                "cols": 100,
            }
        ),
    )
    price = forms.DecimalField(initial=19.99)

    class Meta:
        model = Product
        fields = ["title", "description", "price"]

    # def clean_title(self, *args, **kwargs):
    #     title = self.cleaned_data.get("title")
    #     if not "Try Django" in title:
    #         raise forms.ValidationError("This is not a valid title")
    #     if not "intern" in title:
    #         raise forms.ValidationError("This is not a valid title")
    #     return title
    #     # if "Try Django" in title:
    #     #     return title
    #     # else:
    #     #     raise forms.ValidationError('This is not a valid title')

    # def clean_email(self, *args, **kwargs):
    #     email = self.cleaned_data.get("email")
    #     if not email.endswith("edu"):
    #         raise forms.ValidationError("This is not a valid email")
    #     return email


class RawProductForm(forms.Form):
    title = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "Your title here"})
    )
    description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "placeholder": "Your description",
                "class": "new-class-name two",
                "rows": 20,
                "cols": 100,
            }
        ),
    )
    price = forms.DecimalField(initial=19.99)
